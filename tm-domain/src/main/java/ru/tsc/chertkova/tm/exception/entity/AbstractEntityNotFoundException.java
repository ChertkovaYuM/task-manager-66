package ru.tsc.chertkova.tm.exception.entity;

import ru.tsc.chertkova.tm.exception.AbstractException;

public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException() {
        super();
    }

    public AbstractEntityNotFoundException(String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
