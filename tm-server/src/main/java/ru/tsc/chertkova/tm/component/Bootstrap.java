package ru.tsc.chertkova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.service.*;
import ru.tsc.chertkova.tm.endpoint.*;
import ru.tsc.chertkova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap {

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private Backup backup;

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = String.valueOf(propertyService.getServerPort());
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run() {
        initPID();
        initJMS();
        initEndpoints();
        loggerService.info("** TASK MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        //backup.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER SERVER STOPPED **");
        //backup.stop();
    }

    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    @SneakyThrows
    public void initJMS() {
        BasicConfigurator.configure();
        @NotNull final BrokerService brokerService = new BrokerService();
        brokerService.addConnector("tcp://localhost:8161");
        brokerService.start();
    }

}
