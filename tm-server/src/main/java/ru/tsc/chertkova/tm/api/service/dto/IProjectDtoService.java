package ru.tsc.chertkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.model.ProjectDTO;
import ru.tsc.chertkova.tm.dto.model.TaskDTO;
import ru.tsc.chertkova.tm.enumerated.Status;

import java.util.List;

public interface IProjectDtoService {

    @Nullable
    ProjectDTO add(@Nullable ProjectDTO project);

    @Nullable
    ProjectDTO updateById(
            @Nullable String id,
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description);

    @Nullable
    ProjectDTO changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status);

    boolean existsById(@Nullable String id);

    @Nullable
    ProjectDTO findById(@Nullable String userId,
                        @Nullable String id);

    ProjectDTO removeById(@Nullable String userId,
                          @Nullable String id);

    ProjectDTO remove(@Nullable String userId,
                      @Nullable ProjectDTO project);

    int getSize(@Nullable String userId);

    void clear(@Nullable String userId);

    @Nullable
    List<ProjectDTO> findAll(@Nullable String userId);

    @Nullable
    List<ProjectDTO> addAll(@NotNull List<ProjectDTO> projects);

    @Nullable
    List<ProjectDTO> removeAll(@Nullable List<ProjectDTO> projects);

    @Nullable
    TaskDTO bindTaskToProject(@Nullable String userId,
                              @Nullable String projectId,
                              @Nullable String taskId);

}
