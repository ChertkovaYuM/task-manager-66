package ru.tsc.chertkova.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.api.service.model.IUserService;
import ru.tsc.chertkova.tm.exception.entity.UserNotFoundException;
import ru.tsc.chertkova.tm.exception.field.*;
import ru.tsc.chertkova.tm.exception.user.LoginEmptyException;
import ru.tsc.chertkova.tm.exception.user.LoginExistsException;
import ru.tsc.chertkova.tm.exception.user.PasswordEmptyException;
import ru.tsc.chertkova.tm.model.User;
import ru.tsc.chertkova.tm.repository.model.UserRepository;
import ru.tsc.chertkova.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable User user = userRepository.findByLogin(login);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @Nullable User user = userRepository.findByEmail(email);
        return user;
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        return userRepository.existsById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User remove(@Nullable final User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(userRepository.findById(user.getId()))
                .orElseThrow(UserNotFoundException::new);
        removeById(user.getId());
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        User user = Optional.ofNullable(userRepository.findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        removeById(user.getId());
        return user;
    }

    @Override
    @Nullable
    @Transactional
    public User removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @Nullable User user = userRepository.getOne(id);
        userRepository.deleteById(id);
        return user;
    }

    @Override
    @Transactional
    public void clear(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        userRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public boolean isLoginExists(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        long count = userRepository.isLoginExist(login);
        return count > 0;
    }

    @Override
    @SneakyThrows
    @Transactional
    public boolean isEmailExists(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        long count = userRepository.isEmailExist(email);
        return count > 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User setPassword(@Nullable final String userId,
                            @Nullable final String password) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);

        Optional.ofNullable(userRepository.findById(userId)).orElseThrow(UserNotFoundException::new);
        userRepository.setPassword(userId, HashUtil.salt(propertyService, password));
        @Nullable User user = userRepository.getOne(userId);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public User updateUser(@Nullable final String userId,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) {
        Optional.ofNullable(userId).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(firstName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(middleName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(lastName).orElseThrow(NameEmptyException::new);
        @Nullable User user = Optional.ofNullable(userRepository.getOne(userId))
                .orElseThrow(UserNotFoundException::new);
        userRepository.save(user);
        user = userRepository.getOne(userId);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public User lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        userRepository.setLockedFlag(login, true);
        @Nullable User user = userRepository.findByLogin(login);
        return user;
    }

    @Override
    @Nullable
    @Transactional
    public User unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        userRepository.setLockedFlag(login, false);
        @Nullable User user = userRepository.findByLogin(login);
        return user;
    }

    @Override
    @Nullable
    @Transactional
    public User add(@Nullable final User user) {
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(user.getLogin()).orElseThrow(LoginEmptyException::new);
        Optional.ofNullable(user.getEmail()).orElseThrow(EmailEmptyException::new);
        Optional.ofNullable(user.getPasswordHash()).orElseThrow(PasswordEmptyException::new);
        if (userRepository.isLoginExist(user.getLogin()) > 0) throw new LoginExistsException();
        if (userRepository.isEmailExist(user.getEmail()) > 0) throw new EmailExistsException();
        userRepository.saveAndFlush(user);
        return user;
    }

    @Override
    @Nullable
    @Transactional
    public User updateById(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String middleName,
                           @Nullable final String lastName) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(firstName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(middleName).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(lastName).orElseThrow(NameEmptyException::new);
        @Nullable User user = Optional.ofNullable(userRepository.getOne(id))
                .orElseThrow(UserNotFoundException::new);
        userRepository.save(user);
        user = userRepository.getOne(id);
        return user;
    }

    @Override
    @Nullable
    @Transactional
    public User findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @Nullable User user = userRepository.getOne(id);
        return user;
    }

    @Override
    @Transactional
    public int getSize(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @Nullable int count = Integer.parseInt(String.valueOf(userRepository.count()));
        return count;
    }

    @Override
    @Nullable
    @Transactional
    public List<User> findAll(@Nullable String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @Nullable List<User> users = userRepository.findAll();
        return users;
    }

    @Override
    @Nullable
    @Transactional
    public List<User> addAll(@Nullable List<User> users) {
        Optional.ofNullable(users).orElseThrow(UserNotFoundException::new);
        for (User user : users) {
            Optional.ofNullable(user.getLogin()).orElseThrow(LoginEmptyException::new);
            Optional.ofNullable(user.getEmail()).orElseThrow(EmailEmptyException::new);
            Optional.ofNullable(user.getPasswordHash()).orElseThrow(PasswordEmptyException::new);
            userRepository.save(user);
        }
        return users;
    }

    @Override
    @Nullable
    @Transactional
    public List<User> removeAll(@Nullable final List<User> users) {
        Optional.ofNullable(users).orElseThrow(UserNotFoundException::new);
        userRepository.deleteAll(users);
        return users;
    }

}
