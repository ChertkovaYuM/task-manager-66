package ru.tsc.chertkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.repository.IProjectRepository;
import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.model.Project;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public final class ProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    public Project create() {
        @NotNull final Project project = new Project(
                "Новый проект: " + System.currentTimeMillis(),
                Status.NOT_STARTED,
                new Date());
        save(project);
        return project;
    }

    public Project save(@NotNull final Project project) {
        return repository.save(project);
    }

    @Nullable
    public Collection<Project> findAll() {
        return repository.findAll();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return repository.findById(id).orElse(null);
    }

    public void removeById(@NotNull final String id) {
        repository.deleteById(id);
    }

    public void remove(@NotNull final Project project) {
        repository.delete(project);
    }

    public void remove(@NotNull final List<Project> projects) {
        projects
                .stream()
                .forEach(this::remove);
    }

    public boolean existsById(@NotNull final String id) {
        return repository.existsById(id);
    }

    public void clear() {
        repository.deleteAll();
    }

    public long count() {
        return repository.count();
    }

}
